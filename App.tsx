import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { PanResponder, StyleSheet, Text, View } from 'react-native';

import Header from './src/components/Header';
import ProductList from './src/components/ProductList'

import axios from 'axios';

export default class App extends React.Component {

  constructor(props: {} | Readonly<{}>){
    super(props);

    this.state = {
      produtos: []
    }
  }

  componentDidMount(){
    axios
    .get('http://192.168.0.11:9000/api/items')
    .then(response => {
      const { payload } = response.data;
      this.setState({
        produtos: payload
      });
    })
  }

  render() {
    return (
      <View>
        <Header title="Produtos:" />
        <ProductList produtos={this.state.produtos} />
      </View>
    );
  }
}

