import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const ProductList = (props: { produtos: any; }) => {

    const { produtos } = props;

    const jsonElements = produtos.map((produto: { id?: any; name?: any; }) => {
        const { name } = produto;
        return (
            <View key={produto.id} style={styles.line}>
                <Text style={styles.lineText}>{name}</Text>
            </View>
        );
    });

    return (
        <View style={styles.container}>
            { jsonElements }
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#e2f9ff'
    },
    line: {
        height: 60,
        borderBottomWidth: 1,
        borderBottomColor: "#bbb",
        alignItems: 'center',
        flexDirection: 'row'

    },
    lineText: {
        fontSize: 20,
        paddingLeft: 15
    }


})

export default ProductList;